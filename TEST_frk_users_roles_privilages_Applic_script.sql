/*
Name        : mum_users_roles_privilages.sql
Date        : 03 Jan 2018
Created by  : Anurag Kale
Environment : Mumbai_dev
About       : Script to create roles, users and define their respective privilages.
*****RUN AS ROLE_mum_elx_application User*******
*/


ALTER DEFAULT PRIVILEGES
FOR ROLE ROLE_FRK_ELX_DEV
IN SCHEMA sch_elxtest
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO ROLE_FRK_ELX_APPLICATION;
