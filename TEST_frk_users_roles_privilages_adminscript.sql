/*
Name        : FRK_users_roles_privilages.sql
Date        : 03 Jan 2018
Created by  : Anurag Kale
Environment : Mumbai_dev
About       : Script to create roles, users and define their respective privilages.
*/

/*Script Begins*/

--Create roles
CREATE ROLE ROLE_FRK_ELX_DEV ;
create role ROLE_FRK_ELX_APPLICATION ;
create role ROLE_FRK_ELX_DBA ;

REVOKE ALL ON ALL TABLES IN SCHEMA sch_elxtest FROM PUBLIC;

/* Grant DML to Dev */
GRANT SELECT, INSERT, UPDATE, DELETE
ON ALL TABLES IN SCHEMA sch_elxtest
TO ROLE_FRK_ELX_DEV;

GRANT USAGE,SELECT ON ALL SEQUENCES IN
SCHEMA sch_elxtest to ROLE_FRK_ELX_DEV;

GRANT USAGE ON SCHEMA sch_elxtest
TO role_FRK_ELX_dev ;

/* Grant DML to Application User */
GRANT SELECT, INSERT, UPDATE, DELETE
ON ALL TABLES IN SCHEMA sch_elxtest
TO ROLE_FRK_ELX_APPLICATION;

GRANT EXECUTE ON ALL FUNCTIONS
IN SCHEMA sch_elxtest
To ROLE_FRK_ELX_APPLICATION;

GRANT USAGE, SELECT ON ALL SEQUENCES IN
SCHEMA sch_elxtest to ROLE_FRK_ELX_APPLICATION;

GRANT USAGE ON SCHEMA sch_elxtest
TO ROLE_FRK_ELX_APPLICATION ;

/* Grant Full access to dba user*/
GRANT ALL PRIVILEGES ON SCHEMA sch_elxtest to ROLE_FRK_ELX_DBA;
GRANT USAGE ON SCHEMA sch_elxtest to ROLE_FRK_ELX_DBA;

/*Create test users to Connect*/

create user elx_test_dev with password 'iqtctestdev@2018';
grant ROLE_FRK_ELX_DEV to elx_test_dev;

create user elx_test_application with password 'iqtc_test_application@2018';
grant ROLE_FRK_ELX_APPLICATION to elx_test_application;

create user iqtc_anuragk with password 'iqtcadmin_dba@2018';
grant ROLE_FRK_ELX_DBA to iqtc_anuragk;
/* Script Ends*/
