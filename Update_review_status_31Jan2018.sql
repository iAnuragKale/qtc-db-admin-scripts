/*
Script to update the status of the reviews to deleted
for which the review data went missing

Date : 31 Jan 2018
Created by : Anurag Kale
Email Subject : [ELX] - Products with missing rating
*/

select review_id,review_status_id
from tbl_review
where review_id in (27,23,21)
and review_status_id = 3;

update tbl_review
set review_status_id = 8
where review_id in (27,23,21)
and review_status_id = 3;

--Logs after running the scripts on console
elxproddb=> select review_id,review_status_id
elxproddb-> from tbl_review
elxproddb-> where review_id in (27,23,21)
elxproddb-> and review_status_id = 3;
 review_id | review_status_id
-----------+------------------
        27 |                3
        23 |                3
        21 |                3
(3 rows)

elxproddb=>

elxproddb=>
elxproddb=> update tbl_review
elxproddb-> set review_status_id = 8
elxproddb-> where review_id in (27,23,21)
elxproddb-> and review_status_id = 3;
UPDATE 3
elxproddb=>
