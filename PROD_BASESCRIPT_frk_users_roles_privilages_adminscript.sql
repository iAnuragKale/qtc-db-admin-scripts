/*
Name        : PROD_FRK_users_roles_privilages.sql
Date        : 04 Jan 2018
Created by  : Anurag Kale
Environment : FRK_PROD
About       : Script to create roles, users and define their respective privilages.
******CONFIDENTIAL DATA, DO NOT SHARE, NOT TO BE ACCESSED BY UNAUTHORISED USERS******
*/

/*Script Begins*/

--Create roles
CREATE ROLE ROLE_FRK_ELX_DEV ;
create role ROLE_FRK_ELX_APPLICATION ;
create role ROLE_FRK_ELX_DBA ;

REVOKE ALL ON ALL TABLES IN SCHEMA sch_elxprod FROM PUBLIC;

/* Grant DML to Dev */
GRANT SELECT, INSERT, UPDATE, DELETE
ON ALL TABLES IN SCHEMA sch_elxprod
TO ROLE_FRK_ELX_DEV;

GRANT USAGE,SELECT ON ALL SEQUENCES IN
SCHEMA sch_elxprod to ROLE_FRK_ELX_DEV;

GRANT USAGE ON SCHEMA sch_elxprod
TO role_FRK_ELX_dev ;

/* Grant DML to Application User */
GRANT SELECT, INSERT, UPDATE, DELETE
ON ALL TABLES IN SCHEMA sch_elxprod
TO ROLE_FRK_ELX_APPLICATION;

GRANT EXECUTE ON ALL FUNCTIONS
IN SCHEMA sch_elxprod
To ROLE_FRK_ELX_APPLICATION;

GRANT USAGE, SELECT ON ALL SEQUENCES IN
SCHEMA sch_elxprod to ROLE_FRK_ELX_APPLICATION;

GRANT USAGE ON SCHEMA sch_elxprod
TO ROLE_FRK_ELX_APPLICATION ;

/* Grant Full access to dba user*/
GRANT ALL PRIVILEGES ON SCHEMA sch_elxprod to ROLE_FRK_ELX_DBA;
GRANT USAGE ON SCHEMA sch_elxprod to ROLE_FRK_ELX_DBA;

/*Create test users to Connect*/

/*create user elx_prod_dev with password 'm8KONrg4HXb1H_nn';
grant ROLE_FRK_ELX_DEV to elx_prod_dev;*/

create user elx_prod_application with password 'M&U%86pVnB20-R&f';
grant ROLE_FRK_ELX_APPLICATION to elx_prod_application;

create user iqtc_anuragk with password 'Ys~aAoO9?0qFTecRL7I0';
grant ROLE_FRK_ELX_DBA to iqtc_anuragk;

grant select on all tables in schema sch_elxprod to  elx_prod_read_only;
/* Script Ends*/
